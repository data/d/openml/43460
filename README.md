# OpenML dataset: Apple-Historical-Dataset

https://www.openml.org/d/43460

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

If you reach this DATASET, please UPVOTE this dataset to show your appreciation
DATASET DETAILS:
  
1.Previous Close:        264.29,
2.Open:                265.58,
3.Bid:                266.06 x 800,
4.Ask:                266.06 x 900,
5.Day's Range:            265.31 - 266.27,
6.52 Week Range:        142.00 - 268.00,
7.Volume:                    3,772,399,
8.Avg. Volume:            25,464,369,
9.Market Cap:            1.201T,
10.Beta (3Y Monthly):        1.25,
11.PE Ratio (TTM):        22.35,
12.EPS (TTM):            11.89,
13.Earnings Date:                Jan 27, 2020 - Jan 31, 2020,
14.Forward Dividend  Yield:    3.08 (1.16),
15.Ex-Dividend Date:                2019-11-07,

1y Target Est:                         257.21,
ESG plot


THIS DATASET CAN BE REALLY GOOD FOR BEGINNERS WHO WANT TO TEST THERE SKILLS IN FINANCE AND MACHINE LEARNING, GET STARTED WITH LSTMs MODELLING OR DEEP REINFORCEMENT LEARNING AGENTS BEST FOR THIS DATASETS
TRY YOUR FIRST KERNEL NOW! ALL THE BSET

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43460) of an [OpenML dataset](https://www.openml.org/d/43460). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43460/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43460/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43460/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

